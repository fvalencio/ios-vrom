
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Canvas
#define COCOAPODS_POD_AVAILABLE_Canvas
#define COCOAPODS_VERSION_MAJOR_Canvas 0
#define COCOAPODS_VERSION_MINOR_Canvas 1
#define COCOAPODS_VERSION_PATCH_Canvas 2

// Foursquare-API-v2
#define COCOAPODS_POD_AVAILABLE_Foursquare_API_v2
#define COCOAPODS_VERSION_MAJOR_Foursquare_API_v2 1
#define COCOAPODS_VERSION_MINOR_Foursquare_API_v2 4
#define COCOAPODS_VERSION_PATCH_Foursquare_API_v2 5

// STTwitter
#define COCOAPODS_POD_AVAILABLE_STTwitter
#define COCOAPODS_VERSION_MAJOR_STTwitter 0
#define COCOAPODS_VERSION_MINOR_STTwitter 1
#define COCOAPODS_VERSION_PATCH_STTwitter 4

// pop
#define COCOAPODS_POD_AVAILABLE_pop
#define COCOAPODS_VERSION_MAJOR_pop 1
#define COCOAPODS_VERSION_MINOR_pop 0
#define COCOAPODS_VERSION_PATCH_pop 6

