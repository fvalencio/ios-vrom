//
//  CadViewController.m
//  vrom
//
//  Created by Fabio Valencio on 11/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import "CadViewController.h"

@interface CadViewController ()

@end

@implementation CadViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _viewControllers = [NSArray arrayWithObjects:@"Cad1ViewController", @"Cad2ViewController", @"Cad3ViewController", @"Cad4ViewController", @"Cad5ViewController", nil];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CadPageViewController"];
    self.pageViewController.dataSource = self;
    
    _firstVC = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[_firstVC];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    _pageControl = [UIPageControl appearance];
    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    _pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    _pageControl.backgroundColor = [UIColor whiteColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{

    if (index >= [_viewControllers count]) {
        return nil;
    } else {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[_viewControllers objectAtIndex:index]];

        return vc;
    }
    
    
    
    return NULL;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSString * ident = viewController.restorationIdentifier;
    NSUInteger index = [_viewControllers indexOfObject:ident];

    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSString * ident = viewController.restorationIdentifier;
    NSUInteger index = [_viewControllers indexOfObject:ident];
    
    index++;
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [_viewControllers count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return _index;
}

@end
