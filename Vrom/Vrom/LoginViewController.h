//
//  LoginViewController.h
//  vrom
//
//  Created by Fabio Valencio on 18/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface LoginViewController : UIViewController <FBLoginViewDelegate, UITextFieldDelegate, UIActionSheetDelegate>

@property (nonatomic, retain) IBOutlet UITextField *email;
@property (nonatomic, retain) IBOutlet UITextField *senha;

@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet UIButton *social;

@property (weak, nonatomic) IBOutlet UILabel *termos;
@property (weak, nonatomic) IBOutlet UILabel *politica;
@property (weak, nonatomic) IBOutlet FBLoginView *loginButton;

@property (nonatomic, retain) IBOutlet NSArray *arrayOfAccounts;

- (IBAction)openActionSheet:(id)sender;
- (IBAction)login:(id)sender;
-(void)loginWithFoursquare:(id)userResponse;

@end
