//
//  PopCustomSegue.m
//  vrom
//
//  Created by Fabio Valencio on 07/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import "PopCustomSegue.h"
#import <Pop/POP.h>

@implementation PopCustomSegue

-(void)perform
{
    UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    UIViewController *destinationViewController = (UIViewController*)[self destinationViewController];
    
    CALayer *layer = destinationViewController.view.layer;
    [layer pop_removeAllAnimations];
    
    POPSpringAnimation *xAnim = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
    POPSpringAnimation *sizeAnim = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerSize];
    
    xAnim.fromValue = @(320);
    xAnim.springBounciness = 16;
    xAnim.springSpeed = 10;
    
    sizeAnim.completionBlock = ^(POPAnimation *anim, BOOL finished) {
        NSLog(@"Working");
    };
    
    [layer pop_addAnimation:xAnim forKey:@"position"];
    [layer pop_addAnimation:sizeAnim forKey:@"size"];
    
    [sourceViewController.navigationController pushViewController:destinationViewController animated:NO];
}

@end
