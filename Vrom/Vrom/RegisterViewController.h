//
//  RegisterViewController.h
//  vrom
//
//  Created by Fabio Valencio on 18/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *senha;
@property (weak, nonatomic) IBOutlet UIButton *login;
- (IBAction)register:(id)sender;

@end
