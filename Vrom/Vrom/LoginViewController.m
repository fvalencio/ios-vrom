//
//  LoginViewController.m
//  vrom
//
//  Created by Fabio Valencio on 18/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import "LoginViewController.h"
#import "Foursquare2.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "STTwitter.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize email, senha;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openActionSheet:(id)sender {
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:
                            @"Foursquare",
                            @"Facebook",
                            @"Twitter",
                            @"Email",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (IBAction)login:(id)sender {
    //login api
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1:
        {
            switch (buttonIndex) {
                case 0:
                    if ([Foursquare2 isAuthorized]) {
                        [Foursquare2  userGetDetail:@"self"
                                           callback:^(BOOL secondSuccess, id secondResult){
                                               if (secondSuccess) {
                                                   
                                                   NSMutableDictionary *userResponse = [[NSMutableDictionary alloc] init];
                                                   [userResponse setValue:[[secondResult objectForKey:@"response"] objectForKey:@"user"] forKeyPath:@"user"];
                                                   
                                                   [self loginWithFoursquare:userResponse];
                                               }
                                           }];
                    } else {
                        [Foursquare2 authorizeWithCallback:^(BOOL firstSuccess, id firstResult) {
                            if (firstSuccess) {
                                [Foursquare2  userGetDetail:@"self"
                                                   callback:^(BOOL secondSuccess, id secondResult){
                                                       if (secondSuccess) {
                                                           NSMutableDictionary *userResponse = [[NSMutableDictionary alloc] init];
                                                           [userResponse setValue:[[secondResult objectForKey:@"response"] objectForKey:@"user"] forKeyPath:@"user"];
                                                           
                                                           [self loginWithFoursquare:userResponse];
                                                       }
                                                   }];
                            }
                        }];
                    }
                    break;
                case 1:
                    self.loginButton.delegate = self;
                    self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
                    [self.loginButton.subviews[0] sendActionsForControlEvents:UIControlEventTouchUpInside];
                    
                    break;
                case 2:
                    //STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"RcO3X5yzxeUoEQHnBU5NKGFJf" consumerSecret:@"bUnPMEGM9s4BQYcudd0mgtuJHtDEAVnEdHwIORztRwJekxStjP"];
                    
                    NSLog(@"Twitter");
                    
//                    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"RcO3X5yzxeUoEQHnBU5NKGFJf" consumerSecret:@"bUnPMEGM9s4BQYcudd0mgtuJHtDEAVnEdHwIORztRwJekxStjP"];
//                    [twitter verifyCredentialsWithSuccessBlock:^(NSString *username) {
//                        NSLog(@"%@", username);
//                        [twitter getUserInformationFor:username successBlock:^(NSDictionary *user) {
//                            NSLog(@"%@", user);
//                        } errorBlock:^(NSError *error) {
//                            NSLog(@"%@", error.debugDescription);
//                        }];
//                    } errorBlock:^(NSError *error) {
//                        NSLog(@"%@", error.debugDescription);
//                    }];
                    
                    
                    break;
                case 3:
                    [self performSegueWithIdentifier:@"register" sender:self];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

#pragma FACEBOOK

-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView{
    
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *users = [[NSMutableDictionary alloc] init];
    
    [users setValue:[user objectForKey:@"first_name"] forKeyPath:@"first_name"];
    [users setValue:[user objectForKey:@"last_name"] forKeyPath:@"last_name"];
    [users setValue:[user objectForKey:@"gender"] forKeyPath:@"gender"];
    [users setValue:[user objectForKey:@"email"] forKeyPath:@"email"];
    [users setValue:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture", [user objectForKey:@"id"]] forKeyPath:@"photo"];
    [users setValue:[user objectForKey:@"id"] forKeyPath:@"id"];
    [users setValue:@"Facebook" forKeyPath:@"type_id"];
    
    [defaults setObject:users forKey:@"userDetail"];
    [defaults synchronize];
    // NSLog(@"%@", users);
    //[actionSheet  dismissWithClickedButtonIndex:0 animated:YES];
    [self performSegueWithIdentifier:@"register" sender:self];
}

-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user"];
}

-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}

-(void)loginWithFoursquare:(NSDictionary *)userResponse
{
    //NSLog(@"%@", [userResponse objectForKey:@"user"]);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *user = [[NSMutableDictionary alloc] init];
    
    [user setValue:[[userResponse objectForKey:@"user"] objectForKey:@"firstName"] forKeyPath:@"first_name"];
    [user setValue:[[userResponse objectForKey:@"user"] objectForKey:@"lastName"] forKeyPath:@"last_name"];
    [user setValue:[[userResponse objectForKey:@"user"] objectForKey:@"gender"] forKeyPath:@"gender"];
    [user setValue:[[[userResponse objectForKey:@"user"] objectForKey:@"contact"] objectForKey:@"email"] forKeyPath:@"email"];
    [user setValue:[[userResponse objectForKey:@"user"] objectForKey:@"photo"] forKeyPath:@"photo"];
    [user setValue:[[userResponse objectForKey:@"user"] objectForKey:@"id"] forKeyPath:@"id"];
    [user setValue:@"Foursquare" forKeyPath:@"type_id"];

    [defaults setObject:user forKey:@"userDetail"];
    [defaults synchronize];
    
    [self performSegueWithIdentifier:@"register" sender:self];
}

#pragma - mark TextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 1) {
        [self.email resignFirstResponder];
        [self.senha becomeFirstResponder];
    } else if (textField.tag == 2) {
        [self.senha resignFirstResponder];
    }
    
    
    return YES;
    
}
@end
