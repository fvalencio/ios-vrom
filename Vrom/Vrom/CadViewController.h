//
//  CadViewController.h
//  vrom
//
//  Created by Fabio Valencio on 11/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cad1ViewController.h"
#import "Cad2ViewController.h"
#import "Cad3ViewController.h"
#import "Cad4ViewController.h"
#import "Cad5ViewController.h"

@interface CadViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *viewControllers;
@property (assign, nonatomic) int index;
@property (strong, nonatomic) UIViewController *firstVC;
@property (strong, nonatomic) UIViewController *secondVC;
@property (strong, nonatomic) UIViewController *thirdVC;
@property (strong, nonatomic) UIViewController *fourthVC;
@property (strong, nonatomic) UIViewController *fifthVC;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end
