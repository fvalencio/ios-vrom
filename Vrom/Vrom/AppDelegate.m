//
//  AppDelegate.m
//  vrom
//
//  Created by Fabio Valencio on 07/08/2014.
//  Copyright (c) 2014 Towel Tech. All rights reserved.
//

#import "AppDelegate.h"
#import "Foursquare2.h"
#import <FacebookSDK/FacebookSDK.h>

#define FOURSQUARE_SCHEME @"vrom"
#define FACEBOOK_SCHEME  @"fb617513788364365"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FBLoginView class];
    [Foursquare2 setupFoursquareWithClientId:@"PDXZH14B5AIB403SHWE2XIDT5CS2EHMOZLEIXSP1WDRLRKTL"
                                      secret:@"5R30W3NKPTGYSMNSPULGDPN3BJ45WEZHW4XHW0U4OQCIU0LF"
                                 callbackURL:@"vrom://foursquare"];
    _userDetail = [[NSMutableDictionary alloc] init];
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([[url scheme] isEqualToString:FOURSQUARE_SCHEME])
        return [Foursquare2 handleURL:url];
    
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME])
        return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    return NO;
}

@end
